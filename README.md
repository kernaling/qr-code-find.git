

# 常用二维码识别程序 QrCodeFind 

## 【前言简介】
   - 基于百度 Paddle 飞浆AI图片目标检测框架开发
   - 能识别主流网络上流行的二维码（普通二维码，创意二维码，腾迅小程序，抖音码）
   - 简单易用，与docker一起部署，运行环境不用配置，docker拉下来即能使用！
   - 性能优秀，在 CPU i5 8G内存情况下，平均每张图片 0.065 秒
   - 基于 web 接口，返回 json 结果。
   - 能返回直观的目录检测结果，返回 具体位置框，置信度，框大小。

## 【模型和语料】

      语料训练量约2000张各类的二维码识图片语料
  - 二维码的尺寸基本上只要大于64*64 ，均能正常识别
  - 本程序能自动缓存已经下载过的图片，不用重复下载，有脚本程序定时任务每半小时把过期的缓存图片清理掉
  - 平均每张图片识别时间 < 0.1秒
  - 采用百度自研 PP-PicoDet 网络


## 【docker结构说明】 
- 本二维码识别的 docker 已经安装好相关的环境，实现开箱即用！
- 所有的程序结构说明，所有程序都在 /root/

```
/root/
  |
  |- QrCodeFind/ ## 程序主体
          |
          |- tmp_dir/   ## 主要是接口的临时存在图片的目录，半小时删除过期的图片
          |
          |- py_ktools-master/ ## 这是自己的一个 python 的工具类
          |
          |- logs   ## 存在访问接口的日志，只保存最近七天的数据
          |
          |- model_file_l_416  ##模型准确率最高，速度较慢，平均0.15秒一张图片
          |
          |- model_file_m_416  ##模型准确率次之，速度较快，平均0.06秒一张图片
          |
          |- ppdet ## 百度 PP-PicoDet 模型结构
          |
          |- test_img ## 是待被批量测试的图片
          |
          |- test_img_output ## 输出被检测后的图片
          |
          |- deploy  ## 程序接口部署的目录(只列出主要)
                |
                |- config
                |     |
                |     |- picodet_qr_code.yml #接口的配置文件
                |
                |- python
                      |
                      |- infer_qrcode.py #接口启动的主文章




```

## 【程序部署】

  ***有两种方式部署程序***
  1. 可以使用 git clone --depth=1 https://gitee.com/kernaling/qr-code-find.git 然后自己安装其他组件，其中还需要自己拉取 [py_ktools](https://gitee.com/kernaling/py_ktools)


  2. docker方式，强烈推荐使用此方式!
- 基于 docker debian 直接线上拉取启动即可使用，开箱即用！    
- 拉取镜像： docker search qr-code-find
- 马上启动： docker run -d -p 5001:5001 --name qr-code-find -it huangjiaqideepin/qr-code-find /bin/bash

## 【如何运行】
      第一个测试脚本程序
- **确保准确前要完成的工作**: 拉取 QrCodeFind 的 docker 并成功启动完其 docker后，进入此 docker 内部
```
  1. 执行以下命令，进入 qr-code-find
  #  cd /root/qr-code-find

  2. 执行以下命令，启动一个测试程序
  #  python3 deploy/python/infer.py --model_dir=./model_file_m_416 --image_dir=test_img --output_dir=test_img_output --enable_mkldnn=True --enable_mkldnn_bfloat16=True --cpu_threads=4

    就会有以下输出：
    class_id:3, confidence:0.9582, left_top:[96.20,105.00],right_bottom:[304.34,311.58]
    save result to: test_img_output/0c97d7ce97fa0b5356e82b43b4d7479d.png
    Test iter 12
    class_id:0, confidence:0.9727, left_top:[66.61,118.66],right_bottom:[234.74,288.66]
    save result to: test_img_output/01dba7a94a514f4fa191c015304fffba.png
    Test iter 13
    class_id:2, confidence:0.9842, left_top:[10.61,13.72],right_bottom:[465.95,463.47]
    save result to: test_img_output/2c93772f30aebb10d290757df5f9aef3.png
    Test iter 14
    ------------------ Inference Time Info ----------------------
    total_time(ms): 1130.3000000000002, img_num: 15
    average latency time(ms): 75.35, QPS: 13.270813
    preprocess_time(ms): 25.00, inference_time(ms): 50.30, postprocess_time(ms): 0.00

  3. 完成后，会在 test_img_output 下产生结果，各图片的二维码也会被画上分类和框框，置信度等

```


## 【如何运行】
       建立第一个实用的接口程序
- 同理，先保证把 qr-code-find 拉取，并成功启动后，进入其 docker 内部

- 进入 # cd /root/qr-code-find/
  1. **执行启动命令** # ./shell/start.sh
  2. **执行停止命令** # ./shell/stop.sh

- 启动后，可以尝试查看日志 
  1. **查看日志**  # tail -f /root/logs/info.log

- 访问接口
  1. **访问接口**  # curl 'http://127.0.0.1:5001/qr_code_find?url=http://127.0.0.1:5001/2f857d70da946709d1ea11fbd95fab72.png&test_img=1&rate=0.6'
  2. 参数说明
    ```
      url： 表示对应需要分析的图片url，下载的图片会缓存到 tmp_dir 中，可以写脚本把其定时删除缓存
      rate: 表示置信度取值0~1之间，如：rate=0.6 表示置信度超过0.6才会显示出来。
      test_img：表示是否返回分析后为输出返回二维码框框的URL。
      ### [注意!!] ###
      test_img 此参数只用于图片演示测试，真实分析环境中请取消此参数，因为会影响性能！
    ```
    
 3. 返回的json 分析
  ```
    {
    "stat": 1, ## 状态，只有 stat=1 才会正常状态
    "data": [{  ## 如果一张图片有多个二维码，则数组会多个
      "class_id": 0,  ## 类别id
      "class_name": "qr_common", ## 表示 普通二维码
      "score": 0.9278374314308167, ## 表示是置信度
      "x_min": 203,  ## 位置，左上角 x
      "y_min": 323,  ## 位置，左上角 y
      "x_max": 279,  ## 位置，右下角 x
      "y_max": 397   ## 位置，右下角 y
    }, {
      "class_id": 0,
      "class_name": "qr_common",
      "score": 0.9244049787521362,
      "x_min": 52,
      "y_min": 328,
      "x_max": 124,
      "y_max": 399
    }, {
      "class_id": 0,
      "class_name": "qr_common",
      "score": 0.9178856015205383,
      "x_min": 360,
      "y_min": 324,
      "x_max": 432,
      "y_max": 396
    }],
    "test_img_url": "http://127.0.0.1:5001/d70d1553acfb6fc0b8c78b2b69da1f0b.jpg",  ### 此参数只传入 test_img=1 才会返回，用于演示分析图片二维码标记状态，仅用于测试。
    "msg": "",
    "time": 91  ## 耗用时（下载图片和分析图片总时间）
  }
```
  4. **测试接口性能** 执行并返回以下信息 # ab -n 100 -c 4 'http://127.0.0.1:5001/qr_code_find?url=http://127.0.0.1:5001/2f857d70da946709d1ea11fbd95fab72.png'

  ```
    Concurrency Level:      4
    Time taken for tests:   8.388 seconds
    Complete requests:      100
    Failed requests:        0
    Total transferred:      55900 bytes
    HTML transferred:       43100 bytes
    Requests per second:    11.92 [#/sec] (mean) ## 
    Time per request:       335.534 [ms] (mean)
    Time per request:       83.884 [ms] (mean, across all concurrent requests)
    Transfer rate:          6.51 [Kbytes/sec] received

    Connection Times (ms)
                  min  mean[+/-sd] median   max
    Connect:        0    0   0.0      0       0
    Processing:   194  334  68.9    324     527
    Waiting:      193  334  68.7    324     527
    Total:        194  335  68.9    324     527
  ```
 
  5. 以上参数表示 4条线程，测试 100 次

